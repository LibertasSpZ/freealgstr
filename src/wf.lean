import hott.algebra.relation .tc .trc

open hott.eq hott.trc hott.nat nat

universes u v w

hott_theory

namespace hott

@[hott] inductive acc {A : Type u} (R : A → A → Type v) : A → Type (max u v)
| intro : Π x, (Π y, R y x → acc y) → acc x

namespace acc
  variables {A : Type u} {R : A → A → Type (max u v)}

@[hott] def acc_eq {a : A} (H₁ H₂ : acc R a) : H₁ = H₂ :=
begin
  hinduction H₁ with a K₁ IH₁,
  hinduction H₂ with a K₂ IH₂,
  apply eq.ap (intro a),
  apply eq_of_homotopy, intro a,
  apply eq_of_homotopy, intro r,
  apply IH₁
end

@[hott] def inv {R : A → A → Type v} {x y : A} (H₁ : acc R x) (H₂ : R y x) : acc R y :=
acc.rec_on H₁ (λ x₁ ac₁ iH H₂, ac₁ y H₂) H₂

@[hott] protected def functor {R : A → A → Type v} (S : A → A → Type w)
  (H : ∀ x y, R x y → S x y) (x : A) : acc S x → acc R x :=
by { intro acS, hinduction acS, fconstructor, intros y r, exact ih y (H _ _ r) }

@[hott, elab_as_eliminator] protected def drec
  {C : Π (a : A), acc R a → Type}
  (h₁ : Π (x : A) (acx : Π (y : A), R y x → acc R y),
          (Π (y : A) (ryx : R y x), C y (acx y ryx)) → C x (acc.intro x acx))
  {a : A} (h₂ : acc R a) : C a h₂ :=
acc.rec h₁ h₂

@[hott, class] inductive well_founded {A : Type u}
  (R : A → A → Type v) : Type (max u v)
| intro : (Π a, acc R a) → well_founded

@[hott] def wf_acc {R : A → A → Type v}  (wf : well_founded R) : Πa, acc R a :=
λ  a, well_founded.rec_on wf (λp, p) a

-- CvW Lemma 21
@[hott, elab_as_eliminator] def wf.elim {R : A → A → Type v}
  {C : A → Type w} (Hwf : well_founded R)
  (a : A) (H : Πx, (Πy, R y x → C y) → C x) : C a :=
acc.rec_on (wf_acc Hwf a) (λ x₁ ac₁ iH, H x₁ iH)

@[hott] def wf.functor {R : A → A → Type v} 
  (S : A → A → Type w) (H : ∀ x y, R x y → S x y) :
  well_founded S → well_founded R :=
λ wf, well_founded.intro (λ x, acc.functor S H _ (wf_acc wf _))

@[hott] def wf_comp {B : Type w} (f : A → B) {R : B → B → Type v} :
  well_founded R → well_founded (λ a0 a1, R (f a0) (f a1)) :=
begin
  intro H, fconstructor,
  have H2 : ∀ (b : B) (a : A) (e : f a = b), acc (λ a0 a1, R (f a0) (f a1)) a,
  { intro b, apply wf.elim H b, intros b' H' a e, fconstructor,
    intros a' r, fapply H', exact f a', rwr ←e, exact r, refl },
  intro a, exact H2 (f a) a idp
end

-- Well-foundedness of the natural numbers
@[hott] def wf_nat_lt : well_founded nat.lt :=
begin
  fconstructor, intro n, apply nat.strong_rec_on n, clear n,
  intros n ih, fconstructor, exact ih
end

@[hott] def wf_ulift {R : A → A → Type v} (H : well_founded R) : 
  well_founded (λ x y, ulift.{w} (R x y)) :=
begin
  apply @wf.functor A (λ x y, ulift.{w} (R x y)) R _ H,
  intros a b r, hinduction r, assumption
end

-- CvW Lemma 23
@[hott] def acc_tc {z : A} (ac : acc R z) : acc (tc R) z :=
begin
  hinduction ac with x acx ih, fconstructor, intros y r,
  hinduction r,
  { apply ih, assumption },
  { dsimp at *, fapply acc.inv, exact b, apply ih_1, assumption, assumption }
end

-- CvW Corollary 26
@[hott, instance] def wf_tc_of_wf (wf : well_founded R) : well_founded (tc R) :=
well_founded.intro (λ a, acc_tc (wf_acc wf a))

-- CvW Lemma 22
@[hott] def acc_irreflexive {x : A} (ac : acc R x) : ¬ (R x x) :=
begin
  intro r, hinduction ac, dsimp at*, apply ih x r r,
end

@[hott] def wf_irreflexive (wf : well_founded R) (x : A) : ¬ (R x x) :=
begin
  hinduction wf with ac, apply acc_irreflexive, apply ac  
end

-- CvW Lemma 41 for a well-foundedn relation
@[hott] def acc_tc_span_filter {a : A} (ac : acc R a) (r : tc (sc R) a a) :
  has_span R r :=
begin
  hinduction span_filter R r with eHs Hs Hi,
  { assumption },
  hinduction Hi with Hi Hd,
  { apply empty.elim, apply acc_irreflexive (acc_tc ac) (tc_of_increasing R r Hi) },
  { apply empty.elim, apply acc_irreflexive (acc_tc ac) (tc_of_decreasing R r Hd) }
end

-- CvW Lemma 41 for a Noetherian relation
@[hott] def rev_acc_tc_span_filter {a : A} (ac : acc (λ a b, R b a) a)
  (r : tc (sc R) a a) : has_span R r :=
begin
  hinduction span_filter R r with eHs Hs Hi,
  { assumption },
  hinduction Hi with Hi Hd,
  { apply empty.elim, apply acc_irreflexive (acc_tc ac),
    apply tc_swap, apply tc_of_increasing R r Hi },
  { apply empty.elim, apply acc_irreflexive (acc_tc ac),
    apply tc_swap, apply tc_of_decreasing R r Hd }
end

end acc

end hott
