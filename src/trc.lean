import hott.algebra.relation hott.types.sigma hott.types.pi .util

open hott hott.trunc hott.is_equiv hott.quotient hott.equiv hott.eq
open hott.is_trunc hott.pi hott.relation

universes u v w w'

hott_theory

namespace hott
namespace trc
  variables {A : Type u} (R : A → A → Type (max u v))

@[hott] inductive sc : A → A → Type (max u v)
| pos : ∀ {a b}, R a b → sc a b
| neg : ∀ {a b}, R b a → sc a b

@[hott] def sc.inv {a b : A} (s : sc R a b) : sc R b a :=
by { hinduction s, apply sc.neg, assumption, apply sc.pos, assumption }

@[hott] inductive trc : A → A → Type (max u v)
| refl  : ∀ {a}, trc a a
| snoc : ∀ {a b c}, trc a b → R b c → trc a c

@[hott] def trc_of_eq {a b : A} (p : a = b) : trc R a b :=
by { hinduction p, apply trc.refl }

@[hott] def trc.base : ∀ {a b}, R a b → trc R a b :=
λ a b r, trc.snoc (trc.refl R) r

@[hott] def trc.trans : ∀ {a b c}, trc R a b → trc R b c → trc R a c :=
begin
  intros a b c r s, hinduction s, assumption,
  apply trc.snoc, apply ih, assumption, assumption,
end

local notation c`::`r := trc.snoc c r
local notation c`:::`:100d := trc.trans _ c d
local notation `R*` := trc R

@[hott] def trc_snoc_assoc {a b c d} (p : R* a b) (q : R* b c) (r : R c d) :
  (p ::: q) :: r = p ::: (q :: r) :=
by { hinduction q with r, refl, refl }

@[hott] def trc_refl_append {a b : A} (r : trc R a b) :
  ((trc.refl _) ::: r) = r :=
by { hinduction r, refl, rwr [←trc_snoc_assoc, ih] }

@[hott] def trc_assoc {a b c d} (p : R* a b) (q : R* b c) (r : R* c d) :
  ((p ::: q) ::: r) = p ::: (q ::: r) :=
begin
  hinduction r, refl,
  have := ih q, rwr ←trc_snoc_assoc, rwr this
end

@[hott] def trc_length {a b} (p : R* a b) : ℕ :=
by { hinduction p, exact 0, exact ih + 1 }

@[hott] def trc_length_append {a b c} (p : R* a b) (q : R* b c) :
  trc_length R (p ::: q) = (trc_length R p) + (trc_length R q) :=
by { hinduction q, refl, change (trc_length R (p ::: a_2)) + 1 = _, rwr ih }

@[hott] def trc_hd_cod {a b} (p : R* a b) {c} (r : R b c) : A :=
by { revert c r, hinduction p, intros, exact c, intros, apply ih, assumption }

@[hott] def trc_hd {a b} (p : R* a b) {c} (r : R b c) : R a (trc_hd_cod R p r) :=
by { revert c r, induction p with a a b c' p s IH; intros, exact r, apply IH }

@[hott] def trc_tl {a b} (p : R* a b) {c} (r : R b c) : R* (trc_hd_cod R p r) c :=
begin
  revert c r, induction p with a a b c' p s IH; intros,
  { fconstructor },
  { exact (IH s) :: r }
end

@[hott] def trc_hd_tl_eq {a b} (p : R* a b) {c} (r : R b c) :
  (p :: r) = trc.base R  (trc_hd R p r) ::: trc_tl R p r :=
begin
  revert c r, induction p with a a b c' p s IH; intros,
  { refl },
  { transitivity, rwr IH s, transitivity, rwr trc_snoc_assoc, refl }
end

@[hott] def trc_base_eq_snoc {a b c} (p : R* a b) (r : R b c) :
  (p ::: (trc.base R r)) = (p :: r) :=
rfl

local notation `Rs*` := trc (sc R)
local notation r`⁺`:1200 := sc.pos r
local notation r`⁻`:1200 := sc.neg r

-- CvW Definition 40
@[hott] inductive has_span {a b : A} : Rs* a b → Type (max u v)
| inner : ∀ {x y z} (c : Rs* a x) (d : Rs* z b)
            (r : R y x) (s : R y z), has_span (((c :: r⁻) :: s⁺) ::: d)
| outer : ∀ {x y} (c : Rs* x y) (r : R a x) (s : R b y),
                  has_span (trc.base _ r⁺ ::: c :: s⁻)

@[hott] inductive is_increasing : Π {a b : A}, (Rs* a b) → Type (max u v)
| base : ∀ a, is_increasing (@trc.refl _ _ a)
| snoc : ∀ a b c (d : Rs* a b) (s : R b c),
           is_increasing d → is_increasing (d :: s⁺)

@[hott] inductive is_decreasing : Π {a b : A}, (Rs* a b) → Type (max u v)
| base : ∀ a, is_decreasing (@trc.refl _ _ a)
| snoc : ∀ a b c (d : Rs* a b) (s : R c b), is_decreasing d → is_decreasing (d :: s⁻)

@[hott] def is_decreasing_append {a b c : A} (r : Rs* a b)
  (Hr : is_decreasing R r) (s : Rs* b c) (Hs : is_decreasing R s) :
  is_decreasing R (r ::: s) :=
begin
  hinduction Hs, exact Hr,
  fconstructor, apply ih, exact Hr,
end

@[hott] def has_span_snoc {a b c : A} (d : Rs* a b) (r : (sc R) b c) :
  has_span R d → has_span R (d :: r) :=
begin
  intro H, hinduction H,
  { rwr trc_snoc_assoc, fconstructor },
  { hinduction r with _ _ r _ _ r,
    { change has_span R (_ ::: trc.refl _), fconstructor },
    { rwr trc_snoc_assoc, fconstructor } }
end

@[hott] def increasing_tail {a b c : A} (rs : Rs* a b) 
   (H : is_increasing R rs) (s : Rs* b c) :
   (Σ (p : a = b), rs = by { hinduction p, exact trc.refl _ })
     ⊎ Σ b' (t : R a b') d', (rs ::: s) = trc.base (sc R) t⁺ ::: d' :=
begin
  hinduction H,
  { apply sum.inl, fconstructor, refl, refl },
  { apply sum.inr,
    have := ih (trc.base (sc R) (s⁺) ::: s_1), hinduction this with H1 H2; clear ih,
    { hinduction H1 with p ers, hinduction p, dsimp at *,
      rwr ers, exact ⟨c_1, s, s_1, idp⟩ },
    { hinduction H2 with b' H, hinduction H with t H, hinduction H with d' e,
      exact ⟨b', t, d', by{ rwr [←trc_base_eq_snoc, trc_assoc, e] }⟩  } }
end

@[hott] def span_of_increasing_snoc {a b c d : A} (rs : Rs* a b)
  (r : (sc R) b c) (s : R d c) (H : is_increasing R (rs :: r)) :
  has_span R ((rs :: r) :: s⁻) :=
begin
  revert H, hgeneralize ersr : rs :: r = rsr, intro H,  hinduction H,
  { apply empty.elim, 
    have := ap (trc_length (sc R)) ersr,
    exact hott.nat.succ_ne_zero _ this },
  { have tl := increasing_tail R d_1 a_1 (trc.base _ s⁺),
    change has_span R ((d_1 ::: trc.base _ s⁺):: _),
    hinduction tl with H1 H2,
    { hinduction H1 with p e, hinduction p, dsimp at *,
      rwr e, change has_span R ((trc.base (sc R) s⁺) ::: trc.refl _ :: s_1⁻),
      fconstructor },
    { hinduction H2 with b' H, hinduction H with t H, hinduction H with d' e,
      rwr e, fconstructor }  }
end

@[hott] def span_of_decreasing_snoc {a b c d : A} (rs : Rs* a b)
  (r : (sc R) b c) (s : R c d) (H : is_decreasing R (rs :: r)) :
  has_span R ((rs :: r) :: s⁺) :=
begin
  revert H, hgeneralize ersr : rs :: r = rsr, intro H,
  hinduction H,
  { apply empty.elim,
    have := ap (trc_length (sc R)) ersr,
    exact hott.nat.succ_ne_zero _ this },
  { change has_span R (_ ::: trc.refl (sc R)), fconstructor }
end

@[hott] def span_filter {a b : A} (c : Rs* a b) :
  (has_span R c) ⊎ (is_increasing R c) ⊎ (is_decreasing R c):=
begin
  hinduction c with a a b c r s IH,
  { fapply sum.inr, fapply sum.inl, fconstructor },
  { hinduction IH with H IH,
    { apply sum.inl, apply has_span_snoc, assumption },
    { hinduction IH with H IH,
      { hinduction s with b c s b c s,
        { apply sum.inr, apply sum.inl, fconstructor, assumption },
        { hinduction r with a a b' c' r s IH,
          { apply sum.inr, apply sum.inr, fconstructor, fconstructor },
          { apply sum.inl, apply span_of_increasing_snoc, assumption } } },
      { hinduction s with b c s b c s,
        { hinduction r with a a b' c' r s IH,
          { apply sum.inr, apply sum.inl, fconstructor, fconstructor },
          { apply sum.inl, apply span_of_decreasing_snoc, assumption } },
        { apply sum.inr, apply sum.inr, fconstructor, assumption } } } }
end

@[hott] def trc_of_increasing {a b : A} (r : Rs* a b) (H : is_increasing R r) :
  R* a b :=
by { hinduction H, fconstructor, fconstructor, exact b, assumption, assumption }

@[hott] def length_trc_of_increasing {a b : A} (r : Rs* a b) (H : is_increasing R r) :
  trc_length _ (trc_of_increasing R r H) = trc_length _ r :=
begin
  hinduction H, refl,
  change trc_length R (trc_of_increasing R d _) + 1 = _, rwr ih
end

@[hott] def trc_of_decreasing {a b : A} (r : Rs* a b) (H : is_decreasing R r) :
  R* b a :=
by { hinduction H, fconstructor, exact trc.base _ s ::: ih, }

@[hott] def legnth_trc_of_decreasing {a b : A} (r : Rs* a b) (H : is_decreasing R r) :
  trc_length _ (trc_of_decreasing R r H) = trc_length _ r :=
begin
  hinduction H, refl,
  change trc_length R (_ ::: trc_of_decreasing R d _) = _,
  rwr [trc_length_append, ih, hott.nat.add_comm]
end

@[hott] def trsc_of_trc_pos {a b : A} (r : R* a b) : Rs* a b :=
by { hinduction r with a a b c r s ih, fconstructor, exact ih :: s⁺ }

@[hott] def trsc_of_trc_pos_base {a b : A} (r : R a b) :
  trsc_of_trc_pos _ (trc.base _ r) = trc.base _ (sc.pos r) :=
idp

@[hott] def is_increasing_trsc_of_trc_pos {a b : A} (r : R* a b) :
  is_increasing R (trsc_of_trc_pos R r) :=
by { hinduction r, fconstructor, fconstructor, assumption }

@[hott] def trsc_of_trc_neg {a b : A} (r : R* a b) : Rs* b a :=
by { hinduction r with a a b c r s ih, fconstructor, exact trc.base _ s⁻ ::: ih }

@[hott] def is_decreasing_trsc_of_trc_neg {a b : A} (r : R* a b) :
  is_decreasing R (trsc_of_trc_neg R r) :=
begin
  hinduction r, fconstructor,
  change is_decreasing R (_ ::: (trsc_of_trc_neg R a_1)),
  apply is_decreasing_append, fconstructor, fconstructor,
  assumption
end

@[hott] def trsc_of_trc_pos_of_is_increasing {a b : A} (r : Rs* a b)
  (H : is_increasing R r) :
  trsc_of_trc_pos R (trc_of_increasing _ r H) = r :=
begin
  hinduction H, refl,
  change trsc_of_trc_pos R (trc_of_increasing _ _ _) :: _ = _, rwr ih
end

-- These are the _inner_ vertices in _reverse_ order!
@[hott] def rvertices {a b : A} (r : R* a b) : list A :=
begin
  hinduction r with a a b c r s ih,
  { exact [] },
  { exact list.cons c ih }
end

@[hott] def rvertices_append {a b c : A} (r : R* a b) (s : R* b c) :
  rvertices R (r ::: s) = rvertices R s ++ rvertices R r :=
begin
  hinduction s,
  { refl },
  { change list.cons _ (rvertices R (_ ::: _)) = _, rwr ih }
end
 
@[hott] def trsc_inv {a b : A} (r : Rs* a b) : Rs* b a :=
begin
  hinduction r with a a b c r s,
  { fconstructor },
  { exact trc.trans _ (trc.base _ (sc.inv _ s)) ih }
end

@[hott] def trsc_eq_lift {A' : Type w} (f : A → A')
  (h : Π {a b}, R a b → f a = f b) {a b : A} (r : Rs* a b) : f a = f b :=
begin
  hinduction r with a a b c r s,
  { refl },
  { refine ih ⬝ _, hinduction s,
    { apply h, assumption },
    { symmetry, apply h, assumption } }
end

@[hott] def trsc_eq_lift_of_trc_of_eq {A' : Type w} (f : A → A')
  (h : Π {a b}, R a b → f a = f b) {a b : A} (e : a = b) :
  trsc_eq_lift R f @h (trc_of_eq _ e) = ap f e :=
by { hinduction e, refl }

@[hott] def trc_zero_length {a b : A} (r : Rs* a b)
  (e : trc_length (trc.sc R) r = 0) :
  Σ (p : a = b), r = trc_of_eq _ p :=
begin
  hinduction r,
  { exact ⟨idp, idp⟩ },
  { apply empty.elim, apply nat.succ_ne_zero _ e }
end

@[hott] def trsc_eq_lift_base_pos {a b : A} (r : R a b)
  {A' : Type w} (f : A → A') (h : Π {a b}, R a b → f a = f b) :
  trsc_eq_lift R f @h (trc.base _ r⁺) = h r :=
idp_con _

@[hott] def trsc_eq_lift_base_neg {a b : A} (r : R a b)
  {A' : Type w} (f : A → A') (h : Π {a b}, R a b → f a = f b) :
  trsc_eq_lift R f @h (trc.base _ r⁻) = (h r)⁻¹ᵖ :=
idp_con _

@[hott] def trsc_eq_lift_append
  {A' : Type w} (f : A → A') (h : Π {a b}, R a b → f a = f b)
  {a b c : A} (r : Rs* a b) (s : Rs* b c) :
  trsc_eq_lift R f @h (r ::: s) = (trsc_eq_lift R f @h r) ⬝ (trsc_eq_lift R f @h s) :=
begin
  hinduction s with a a b c r' s,
  { refl },
  { hinduction s,
    { change trsc_eq_lift _ _ _ (r ::: r') ⬝ h _ = _, rwr [ih r, con.assoc] },
    { change trsc_eq_lift _ _ _ (r ::: r') ⬝ (h _)⁻¹ᵖ = _, rwr [ih r, con.assoc] } }
end

@[hott] def trsc_eq_lift_inv
  {A' : Type w} (f : A → A') (h : Π {a b}, R a b → f a = f b)
  {a b : A} (r : Rs* a b) :
  trsc_eq_lift R f @h (trsc_inv R r) = (trsc_eq_lift R f @h r)⁻¹ :=
begin
  hinduction r with a a b c r s,
  { refl },
  { change trsc_eq_lift _ _ _ (_ ::: trsc_inv _ _) = _,
    rwr [trsc_eq_lift_append, ih], hinduction s,
    { change _ = (trsc_eq_lift R f @h _ ⬝ h _)⁻¹ᵖ, 
      dsimp[sc.inv], rwr [trsc_eq_lift_base_neg, con_inv] },
    { change _ = (trsc_eq_lift R f @h _ ⬝ (h _)⁻¹ᵖ)⁻¹ᵖ,
      dsimp[sc.inv], rwr [inv_con_inv_right, trsc_eq_lift_base_pos] } }
end

@[hott] def trsc_ap_compose {X : Type w} {Y : Type w'} (f : A → X)
  (h : Π {a b}, R a b → f a = f b) (k : X → Y) (a b) (r : Rs* a b):
  ap k (@trsc_eq_lift A R X f @h a b r)
  = @trsc_eq_lift A R Y (k ∘ f) (λ a b r, ap k (h r)) a b r :=
begin
  hinduction r with a a b c r s,
  { refl },
  { transitivity, apply ap_con, hinduction s with a b s,
    { apply hott.eq.whisker_right, exact ih },
    { change ap k (trsc_eq_lift R f @h r) ⬝ _ = _,
      rwr ih, apply hott.eq.whisker_left, apply ap_inv } }
end

end trc

end hott