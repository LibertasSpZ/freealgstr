import hott.hit.quotient
import hott.types.sigma
import hott.cubical.square
import .util

universes u v w w'

hott_theory

namespace hott
namespace quotient

open hott hott.equiv hott.is_equiv hott.eq hott.quotient hott.sigma

section 
  parameters {A : Type u}
    {R : A → A → Type v}

local notation `D ` := quotient R
local notation `ι ` := class_of R

section
  parameters (x : A)

@[hott] protected structure CatD_ob :=
  (L : D → Type w')
  (n : L (ι x))

@[hott] protected structure CatD_mor (X Y : CatD_ob) :=
  (L : Π d, X.L d → Y.L d)
  (n : L _ X.n = Y.n)

@[hott] protected structure CatD_mor_eq {X Y : CatD_ob} (f g : CatD_mor X Y) :=
  (L : Π d (x : X.L d), f.L d x = g.L d x)
  (n : L _ X.n ⬝ g.n = f.n)

@[hott] private def CatD_mor_eq_to_eq {X Y : CatD_ob} (f g : CatD_mor X Y)
  (e : CatD_mor_eq f g) : f = g :=
begin
  hinduction f with fL fn, hinduction g with gL gn, hinduction e with eL en,
  fapply apd011 CatD_mor.mk,
  fapply eq_of_homotopy2, intros d x,
  apply eL, dsimp at *, apply eq_pathover, 
  rwr [←en, ap_constant, ←apd100_eq_ap_eval, apd100_of_eq_of_homotopy],
  apply square_of_eq, refl
end

@[hott] private def CatD_mor_eq_refl {X Y : CatD_ob} (f : CatD_mor X Y)
  : CatD_mor_eq f f :=
⟨λ d x, idp, idp_con _⟩

@[hott] private def CatD_id (X : CatD_ob) : CatD_mor X X := ⟨λ d y, y, idp⟩

@[hott] private def CatD_comp {X Y Z : CatD_ob}
  (g : CatD_mor Y Z) (f : CatD_mor X Y) : CatD_mor X Z :=
⟨λ d y, g.L _ (f.L _ y), ap _ f.n ⬝ g.n⟩

@[hott] private def CatD_init : CatD_ob := ⟨λ d, ι x = d, idp⟩

@[hott] private def CatD_init_mor (X : CatD_ob) : CatD_mor CatD_init X :=
⟨λ d p, p ▸ X.n, idp⟩

@[hott] private def CatD_initiality (X : CatD_ob) (f g : CatD_mor CatD_init X)
  : CatD_mor_eq f g :=
begin
  fconstructor,
  { intros d p, hinduction p, apply f.n ⬝ g.n⁻¹ },
  { apply con_eq_of_eq_con_inv, refl }
end

@[hott] private def CatD_init_mor_eq_init
  : CatD_mor_eq (CatD_id _) (CatD_init_mor CatD_init) :=
begin
  apply CatD_initiality
end

@[hott] protected structure CatC_ob :=
  (K : A → Type w')
  (n : K x)
  (c : Π y z (r : R y z), K y = K z)

@[hott] protected structure CatC_mor (X Y : CatC_ob) :=
  (K : Π y, X.K y → Y.K y)
  (n : K x X.n = Y.n)
  (c : Π y z r k, K _ (cast (X.c y z r) k) = cast (Y.c y z r) (K _ k))

@[hott] private def CatC_id (X : CatC_ob) : CatC_mor X X :=
⟨λ y, id, idp, λ y z r p, idp⟩

@[hott] private def CatC_comp {X Y Z : CatC_ob}
  (g : CatC_mor Y Z) (f : CatC_mor X Y) : CatC_mor X Z :=
⟨λ y, (g.K y) ∘ (f.K y), ap _ f.n ⬝ g.n, λ y z r p,
  ap _ (f.c _ _ _ _) ⬝ g.c _ _ _ _⟩

@[hott] private def CatCD_ob (X : CatC_ob) : CatD_ob :=
⟨hott.quotient.elim X.K X.c, X.n⟩

@[hott] private def CatDC'_ob (X : CatD_ob) : CatC_ob :=
⟨X.L ∘ ι, X.n, λ y z r, ap X.L (eq_of_rel R r)⟩

@[hott] private def CatCD_ob_CatDC'_ob (X : CatD_ob)
  : CatCD_ob (CatDC'_ob X) = X :=
begin
  hinduction X with L n,
  dsimp[CatCD_ob, CatDC'_ob],
  fapply apd011 CatD_ob.mk,
  apply eq_of_homotopy, intro d, hinduction d, refl,
  apply eq_pathover_dep, dunfold quotient.elim,
  rwr rec_eq_of_rel, rwr apd_eq_pathover_of_eq_ap, exact hrflo,
  apply pathover_eq_of_homotopy, refl
end

@[hott] private def CatCD_ob_equiv : CatC_ob ≃ CatD_ob :=
begin
  fapply equiv.MK (CatCD_ob x) (CatDC'_ob x),
  { apply CatCD_ob_CatDC'_ob },
  { intro X, hinduction X with K n c,
    apply ap (CatC_ob.mk K n), apply eq_of_homotopy3, intros y z r,
    dsimp[CatCD_ob, CatDC'_ob], apply elim_eq_of_rel }
end

@[hott] private def CatCD_mor {X Y : CatC_ob} (f : CatC_mor X Y)
  : CatD_mor (CatCD_ob X) (CatCD_ob Y) :=
begin
  fconstructor; dsimp[CatCD_ob],
  { intro d, hinduction d,
    { exact f.K a },
    { apply pi.arrow_pathover, intros x y p, apply pathover_eq_of_rel_elim,
      transitivity, symmetry, apply f.c, apply ap (f.K a'),
      apply cast_of_pathover_eq_of_rel_elim, exact p } },
  { exact f.n }
end

@[hott] private def CatC_init : CatC_ob := CatDC'_ob CatD_init

@[hott] private def cast_CatC_init (y z : A) (r : R y z) (p : ι x = ι y)
  : cast (CatC_init.c y z r) p = p ⬝ eq_of_rel R r :=
by { apply con_eq_cast_ap }

@[hott] private def CatCD_mor_equiv_step1 (X Y : CatD_ob)
  : CatD_mor X Y
    ≃ Σ g : Π (d : D), X.L d → Y.L d, g _ X.n = Y.n :=
begin
  fapply equiv.MK,
  { intro f, exact ⟨f.1, f.2⟩ },
  { intro g, exact ⟨g.1, g.2⟩ },
  { intro g, hinduction g with g1 g2, refl },
  { intro g, hinduction g with g1 g2, refl }
end

@[hott] private def CatCD_mor_equiv_step2' (X Y : CatD_ob)
  : (Π (d : D), X.L d → Y.L d) 
    ≃ (Σ f : Π (x : A), X.L (ι x) → Y.L (ι x),
        (Π b c s, f b =[eq_of_rel R s; λ x, X.L x → Y.L x] f c)) :=
quotient_pi_equiv _

@[hott] private def CatCD_mor_equiv_step2 (X Y: CatD_ob)
  : (Σ g : Π (d : D), X.L d → Y.L d, g _ X.n = Y.n)
    ≃ Σ (f : Π (x : A), X.L (ι x) → Y.L (ι x))
        (γ : Π b c s, f b =[eq_of_rel R s; λ x, X.L x → Y.L x] f c),
        f _ X.n = Y.n :=
begin
  transitivity, apply sigma_equiv_sigma_left _ (CatCD_mor_equiv_step2' x X Y),
  transitivity, symmetry, apply sigma_assoc_equiv,
  refl
end

@[hott] private def CatCD_mor_equiv_step3 (X Y : CatD_ob)
  : (Σ (f : Π (x : A), X.L (ι x) → Y.L (ι x))
        (γ : Π b c s, f b =[eq_of_rel R s; λ x, X.L x → Y.L x] f c),
        f _ X.n = Y.n)
    ≃ Σ (f : Π (x : A), X.L (ι x) → Y.L (ι x))
        (γ : Π b c (s : R b c) x, f c (cast (ap X.L (eq_of_rel R s)) x)
                        = cast (ap Y.L (eq_of_rel R s)) (f b x)),
        f _ X.n = Y.n :=
begin
  apply sigma_equiv_sigma_right, intro f,
  apply sigma_equiv_sigma_left,
  apply pi.pi_equiv_pi_right, intro b,
  apply pi.pi_equiv_pi_right, intro c,
  apply pi.pi_equiv_pi_right, intro s,
  apply pathover_arrow_equiv_cast_pt
end

@[hott] private def CatCD_mor_equiv_step4 (X Y : CatD_ob)
  : (Σ (f : Π (x : A), X.L (ι x) → Y.L (ι x))
        (γ : Π b c (s : R b c) x, f c (cast (ap X.L (eq_of_rel R s)) x)
                        = cast (ap Y.L (eq_of_rel R s)) (f b x)),
        f _ X.n = Y.n)
    ≃ CatC_mor (CatDC'_ob X) (CatDC'_ob Y) :=
begin
  fapply equiv.MK,
  { intro f, hinduction f with f γ, hinduction γ with γ ε, exact ⟨f, ε, γ⟩ },
  { intro g, hinduction g with g ε γ, exact ⟨g, γ, ε⟩ },
  { intro g, hinduction g with g ε γ, refl },
  { intro f, hinduction f with f γ, hinduction γ with γ ε, refl }
end

@[hott] private def CatCD_mor_equiv (X Y : CatD_ob)
  : CatD_mor X Y ≃ CatC_mor (CatDC'_ob X) (CatDC'_ob Y) :=
   CatCD_mor_equiv_step1 X Y
⬝e CatCD_mor_equiv_step2 X Y
⬝e CatCD_mor_equiv_step3 X Y
⬝e CatCD_mor_equiv_step4 X Y

@[hott] private def CatCD_mor_equiv' (X Y : CatC_ob)
  : CatC_mor X Y ≃ CatD_mor (CatCD_ob X) (CatCD_ob Y) :=
begin
  transitivity,
  have : X = CatDC'_ob x (CatCD_ob x X),
  { symmetry, apply (CatCD_ob_equiv _).to_left_inv }, rwr this,
  transitivity,
  have : Y = CatDC'_ob x (CatCD_ob x Y),
  { symmetry, apply (CatCD_ob_equiv _).to_left_inv }, rwr this,
  symmetry, apply CatCD_mor_equiv
end

@[hott] private def CatC_init_mor (X : CatC_ob) : CatC_mor CatC_init X :=
begin
  have eqv := (CatCD_mor_equiv x (CatD_init x) (CatCD_ob x X)).to_fun (CatD_init_mor x _),
  have : CatDC'_ob x (CatCD_ob x X) = X,
  { apply (CatCD_ob_equiv _).to_left_inv },
  rwr this at eqv, exact eqv
end

@[hott] private def CatCD_ob_init : CatCD_ob CatC_init = CatD_init :=
begin
  dsimp[CatC_init],
  apply CatCD_ob_CatDC'_ob,
end

@[hott] private def CatC_initiality (X : CatC_ob) (f g : CatC_mor CatC_init X)
  : f = g :=
begin
  let f' : CatD_mor (CatD_init x) (CatCD_ob x X),
  { rwr ←CatCD_ob_init, exact (CatCD_mor_equiv' x (CatC_init x) X) f },
  let g' : CatD_mor (CatD_init x) (CatCD_ob x X),
  { rwr ←CatCD_ob_init, exact (CatCD_mor_equiv' x (CatC_init x) X) g },
  have fg' : f' = g',
  { apply CatD_mor_eq_to_eq, apply CatD_initiality },
  have := eq_of_tr_eq_tr fg',
  exact eq_of_fn_eq_fn (CatCD_mor_equiv' x (CatC_init x) X) this,
end

end

section elim'
parameters {Q : A → Type (max u v)}
  (x : A)
  (Qrefl : Q x)
  (Qeq : Π y z (r : R y z), Q y = Q z)
include Qrefl Qeq

@[hott] def Q_obj' : CatC_ob x := ⟨Q, Qrefl, Qeq⟩

@[hott] def path_elim'_raw := CatC_init_mor x Q_obj'

@[hott, elab_as_eliminator] protected def path_elim' (y : A) (p : ι x = ι y) : Q y :=
path_elim'_raw.K _ p

@[hott] def path_elim'_refl : path_elim' x idp = Qrefl := path_elim'_raw.n

@[hott] def path_elim'_cons (y z : A) (p : ι x = ι y) (r : R y z)
 : path_elim' z (p ⬝ eq_of_rel R r) = cast (Qeq y z r) (path_elim' y p) :=
ap (path_elim' z) (con_eq_cast_ap p (eq_of_rel R r))⁻¹ᵖ ⬝ path_elim'_raw.c y _ r p

section elim'_unique
parameters (P : Π (y : A) (p : ι x = ι y), Q y)
  (Prefl : P x idp = Qrefl)
  (Pcons : Π y z r p, P z (cast ((CatC_init x).c y z r) p) = cast (Q_obj'.c y z r) (P y p))
include Prefl Pcons

@[hott] private def P_mor' : CatC_mor x (CatC_init x) Q_obj' 
:= ⟨P, Prefl, Pcons⟩

@[hott] protected def path_elim'_unique_raw : P_mor' = path_elim'_raw :=
CatC_initiality x Q_obj' P_mor' path_elim'_raw

end elim'_unique

end elim'

section rec'
  parameters (x : A)
    {Q : Π (y : A), ι x = ι y → Type (max u v)}
    (Qrefl : Q x (refl (ι x)))
    (Qeq' : Π (y z) (r : R y z) (p : ι x = ι y), Q y p = Q z (p ⬝ eq_of_rel R r))
  include Qrefl Qeq'

open hott.sigma

@[hott] private def Qeq (y z : A) (r : R y z) (p : ι x = ι y) : 
 Q y p = Q z (cast ((CatC_init x).c y z r) p) :=
Qeq' y z r p ⬝ ap (Q z) (cast_CatC_init _ _ _ _ _)⁻¹ᵖ

@[hott] private def sigmaQ' : CatC_ob x :=
⟨λ y, Σ p, Q y p, ⟨idp, Qrefl⟩,
  λ y z r, by { fapply sigma_eq_sigma_of_eq_cast,
                apply (CatC_init x).c y z r,
                symmetry, fapply eq_of_homotopy, intro p, dsimp,
                apply Qeq x Qrefl Qeq' }⟩

@[hott] private def sigmaQ'_mor := CatC_init_mor x sigmaQ'

@[hott] private def proj_mor : CatC_mor x sigmaQ' (CatC_init x) :=
⟨λ y s, s.1, idp, λ y z r p, (cast_sigma_eq_sigma_of_eq_cast' _ _ _)..1⟩

@[hott] private def sigmaQ'_mor_char --F is f1,del1,gam1 later
  {F : CatC_mor x (CatC_init x) (CatC_init x)} (p : CatC_id x _ = F)
  : Σ (p1 : (λ y (q : ι x = ι y), q) = F.K),
      (idp =[apd100 p1⁻¹ x idp; λ c, F.K x idp = c] F.n)
      × (F.c
    =[p1⁻¹; λ (ff : Π y, ι x = ι y → ι x = ι y),
          Π y z r q, ff z (cast ((CatC_init x).c y z r) q) = cast ((CatC_init x).c y z r) (ff y q)]
    (λ (y z : A) (r : R y z) (q : ι x = ι y),
    hott.eq.refl (cast ((CatC_init x).c y z r) q))) :=
by { hinduction p, fconstructor, refl, fconstructor, refl, refl }

@[hott] private def sigmaQ'_mor_unique_char :=
sigmaQ'_mor_char (CatC_initiality x (CatC_init x)
  (CatC_id _ _) (CatC_comp x proj_mor sigmaQ'_mor))

@[hott] private def path_rec'_all_compat
  (f1 : Π y, ι x = ι y → ι x = ι y)
  (p : (λ y (q : ι x = ι y), q) = f1)
  : Π y z r q, f1 z (q ⬝ eq_of_rel R r) = f1 y q ⬝ eq_of_rel R r :=
by { hinduction p, intros, refl }

@[hott] private def path_rec'_all_general
  (f1 : Π y, ι x = ι y → ι x = ι y)
  (f2 : Π y q, Q _ (f1 y q))
  (delta1 : f1 x idp = idp)
  (delta2 : f2 x idp =[delta1] Qrefl)
  (gamma1 : Π y z r q, f1 z (cast ((CatC_init x).c y z r) q) = cast ((CatC_init x).c y z r) (f1 y q))
  (gamma2 : Π (y z : A) r q, f2 z (cast ((CatC_init x).c y z r) q)
    =[gamma1 y z r q] hott.eq.cast (Qeq y z r (f1 y q)) (f2 _ q))
  (psi1 : (λ y (q : ι x = ι y), q) = f1)
  (psi2 : idp =[apd100 psi1⁻¹ᵖ x idp; λ c, f1 x idp = c] delta1)
  (psi3 : gamma1
    =[psi1⁻¹; λ (ff : Π y, ι x = ι y → ι x = ι y),
          Π y z r q, ff z (cast ((CatC_init x).c y z r) q) = cast ((CatC_init x).c y z r) (ff y q)]
    (λ (y z : A) (r : R y z) (q : ι x = ι y),
    hott.eq.refl (cast ((CatC_init x).c y z r) q))) :
  Σ (prec' : Π y p, Q y p), (prec' x idp = Qrefl)
    × (Π (y z : A) (r : R y z) (p : ι x = ι y),
        prec' z (cast ((CatC_init x).c y z r) p) = cast (Qeq y z r _) (prec' y p)) :=
begin
  hinduction psi1, hinduction psi2,
  have psi3' := inverseo psi3, hinduction psi3',
  exact ⟨f2, eq_of_pathover_idp delta2, λ y z r q, eq_of_pathover_idp (gamma2 y z r q)⟩,
end

@[hott] private def path_rec'_all
  : Σ (prec' : Π y p, Q y p), (prec' x idp = Qrefl)
    × (Π (y z : A) (r : R y z) (p : ι x = ι y),
        prec' z (cast ((CatC_init x).c y z r) p) = cast (Qeq y z _ _) (prec' y p)) :=
begin
  refine path_rec'_all_general x Qrefl Qeq' _ _ _ _ _ _ _ _ _,
  { intros y q, exact ((sigmaQ'_mor x Qrefl Qeq').K y q).1 },
  { intros y q, exact ((sigmaQ'_mor x Qrefl Qeq').K y q).2 },
  { exact (sigmaQ'_mor x Qrefl Qeq').n..1 },
  { exact (sigmaQ'_mor x Qrefl Qeq').n..2 },
  { intros y z r q, refine ((sigmaQ'_mor x Qrefl Qeq').c y z r q)..1 ⬝ _,
    exact (cast_sigma_eq_sigma_of_eq_cast' _ _ _)..1, },
  { intros y z r q, refine ((sigmaQ'_mor x Qrefl Qeq').c y z r q)..2 ⬝o _,
    exact (cast_sigma_eq_sigma_of_eq_cast' _ _ _)..2 },
  { exact (sigmaQ'_mor_unique_char x Qrefl Qeq').1 },
  { exact (sigmaQ'_mor_unique_char x Qrefl Qeq').2.1 },
  { exact (sigmaQ'_mor_unique_char x Qrefl Qeq').2.2 }
end

@[hott] protected def path_rec' : Π (y : A) (p : ι x = ι y), Q y p := path_rec'_all.1

@[hott] protected def path_rec'_refl : path_rec' x idp = Qrefl := path_rec'_all.2.1

@[hott] private def path_rec'_cons_aux (y z : A) (p : ι x = ι y) (r : R y z) :=
begin
  have pp := (path_rec'_all x Qrefl Qeq').2.2 y z r p,
  dsimp[Qeq] at pp,
  rwr cast_con (Qeq' y z r p) (ap (Q z) (cast_CatC_init x y z r p)⁻¹) at pp,
  exact pathover_of_pathover_ap _ _ (pathover_of_tr_eq pp⁻¹)
end

@[hott] protected def path_rec'_cons (y z : A) (p : ι x = ι y) (r : R y z)
  : path_rec' z (p ⬝ eq_of_rel R r) = cast (Qeq' y z r p) (path_rec' y p) :=
eq_of_parallel_po_left
  (apd (path_rec' z) (cast_CatC_init x y z r p)⁻¹ᵖ)
  (path_rec'_cons_aux y z p r)

end rec'

section elim
  parameters {Q : A → Type (max u v)}
  (x : A)
  (Qrefl : Q x)
  (Qequiv : Π y z (r : R y z), Q y ≃ Q z)
include Qrefl Qequiv

@[hott, elab_as_eliminator] protected def path_elim
  (y : A) (p : ι x = ι y) : Q y :=
path_elim' x Qrefl (λ y z r, ua (Qequiv y z r)) _ p

@[hott] def path_elim_refl : path_elim x idp = Qrefl :=
path_elim'_refl x Qrefl (λ y z r, ua (Qequiv y z r))

@[hott] def path_elim_cons (y z : A)
  (p : ι x = ι y) (r : R y z)
  : path_elim z (p ⬝ eq_of_rel R r) = Qequiv y z r (path_elim y p) :=
path_elim'_cons x Qrefl (λ y z r, ua (Qequiv y z r)) y z p r
  ⬝ by { rwr cast_ua }

end elim

section rec
  parameters (x : A)
    {Q : Π (y : A), ι x = ι y → Type (max u v)}
    (Qrefl : Q x (refl (ι x)))
    (Qequiv : Π (y z) (r : R y z) (p : ι x = ι y), Q y p ≃ Q z (p ⬝ eq_of_rel R r))
  include Qrefl Qequiv


@[hott, elab_as_eliminator] protected def path_rec
  (y : A) (p : ι x = ι y) : Q y p :=
path_rec' x Qrefl (λ y z r, ua (Qequiv y z r p)) _ p

@[hott] def path_elim_refl : path_elim x idp = Qrefl :=
path_rec'_refl x Qrefl (λ y z r, ua (Qequiv y z r))

@[hott] def path_elim_cons (y z : A)
  (p : ι x = ι y) (r : R y z)
  : path_elim z (p ⬝ eq_of_rel R r) = Qequiv y z r (path_elim y p) :=
path_rec'_cons x Qrefl (λ y z r, ua (Qequiv y z r)) y z p r
  ⬝ by { rwr cast_ua }

end rec

end

end quotient
end hott