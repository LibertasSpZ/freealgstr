import hott.types.trunc
import hott.types.sigma
import hott.function
import hott.hit.quotient
import hott.homotopy.connectedness

universes u v w

hott_theory

open hott hott.trunc hott.function hott.is_equiv hott.quotient hott.sigma hott.equiv hott.eq
open hott.is_trunc hott.pi

namespace hott

@[hott, instance] def is_prop_total_image_of_weakly_constant {A : Type u} {P : Type v}
  [is_set P] (f : A → P) [p : is_weakly_constant f] : is_prop (hott.total_image f) :=
begin
  fapply hott.is_trunc.is_prop.mk, intros x y,
  cases x with x fx, cases y with y fy, hinduction fx with fx, hinduction fy with fy,
  cases fx with a ax, cases fy with b be, induction ax, induction be,
  fapply hott.sigma.sigma_eq, apply p,
  apply hott.is_trunc.is_prop.elimo,
end

@[hott] def prop_trunc.elim_set {A : Type u} {P : Type v} [is_set P] (f : A → P)
  [p : is_weakly_constant f] (x : merely A) : P :=
begin
  have p' : hott.total_image f,
  { hinduction x, fconstructor, exact f a, exact tr ⟨a, refl _⟩ },
  exact p'.1
end

@[hott] def prop_trunc.elim_set_tr {A : Type u} {P : Type v} [is_set P] (f : A → P)
  [p : is_weakly_constant f] (a : A) : prop_trunc.elim_set f (tr a) = f a :=
refl _

@[hott] def tr_eq_eq_con {A : Type u} {x y z : A} (q : x = y) (p : y = z)
  : p ▸ q = q ⬝ p := idp

@[hott] def con_eq_cast_ap {A : Type u} {x y z : A} (p : x = y) (r : y = z)
  : cast (ap (eq x) r) p = p ⬝ r :=
by { hinduction r, refl }

@[hott] def pathover_con_eq_cast_ap {A : Type u} (x : A)
  {P : Π (a : A) (p : x = a), Type v} (F : Π a p, P a p)
  {y z : A} (p : x = y) (r : y = z)
  : F z (p ⬝ r) = (con_eq_cast_ap p r) ▸ F z (cast (ap (eq x) r) p) :=
by { hinduction r, refl }

@[hott] def pathover_eval_of_pathover_apd10 {A : Type u} {a : A} {F F' : A → Type v}
  {H : F = F'} {x : F a} {y : F' a} (p : x =[apd10 H a; id] y)
  : x =[H; λ (F : A → Type v), F a] y :=
begin
  hinduction H, apply pathover_idp_of_eq,
  exact eq_of_pathover_idp p
end

@[hott] def pathover_eq_of_homotopy {A : Type u} {a : A} {F F' : A → Type v}
  {H : homotopy F F'} {x : F a} {y : F' a} (p : x =[H a; id] y)
  : x =[eq_of_homotopy H; λ (F : A → Type v), F a] y :=
begin
  rwr ←apd10_eq_of_homotopy H at p,
  exact pathover_eval_of_pathover_apd10 p
end

@[hott] def fn_cast_eq_cast_fn {A : Type u} {B C : A → Type v}
  {a a' : A} (p : a = a') (f : Π a, B a → C a) (b : B a)
  : f _ (hott.eq.cast (ap B p) b) = hott.eq.cast (ap C p) (f _ b) :=
begin
  hinduction p, refl
end

--TODO replace this by lemma 12
@[hott] def pathover_eq_of_rel_elim {A : Type u} {R : A → A → Type v}
  (Pc : A → Type w)
  (Pp : Π⦃a a' : A⦄ (H : R a a'), Pc a = Pc a') {a a' : A} (H : R a a')
  (x : Pc a) (y : Pc a') (q : cast (Pp H) x = y)
  : by { change Pc a, exact x } =[eq_of_rel R H; quotient.elim Pc Pp] y :=
begin
  apply pathover_of_tr_eq, rwr [tr_eq_cast_ap_fn, elim_eq_of_rel], exact q
end

--TODO replace this by lemma 12
@[hott] def cast_of_pathover_eq_of_rel_elim --TODO this is the inv of the aboved
  {A : Type u} {R : A → A → Type v} (Pc : A → Type w)
  (Pp : Π⦃a a' : A⦄ (H : R a a'), Pc a = Pc a') {a a' : A} (H : R a a')
  (x : Pc a) (y : Pc a')
  (q : by { change Pc a, exact x } =[eq_of_rel R H; quotient.elim Pc Pp] y)
  : cast (Pp H) x = y :=
begin
  have q' := tr_eq_of_pathover q,
  rwr [tr_eq_cast_ap_fn, elim_eq_of_rel] at q',
  exact q'
end

-- PS Lemma 12
@[hott] def pathover_arrow_equiv_cast {A : Type u} {F : A → Type v}
  {G : A → Type w} {a a' : A} (q : a = a') (h : F a → G a) (k : F a' → G a')
  : (h =[q; λ x, F x → G x] k) ≃ (k ∘ cast (ap F q) = cast (ap G q) ∘ h) :=
begin
  hinduction q, transitivity, apply pathover_idp,
  apply eq_equiv_eq_symm
end

@[hott] def pathover_arrow_equiv_cast_pt {A : Type u} {F : A → Type v}
  {G : A → Type w} {a a' : A} (q : a = a') (h : F a → G a) (k : F a' → G a')
  : (h =[q; λ x, F x → G x] k) 
    ≃ Π x, k (cast (ap F q) x) = cast (ap G q) (h x) :=
begin
  transitivity, apply pathover_arrow_equiv_cast,
  apply eq_equiv_homotopy
end

@[hott] def apd100_eq_ap_eval {A : Type u} {P Q : A → Type v}
  {f g : Πx, P x → Q x} (H : f = g) (a : A) (b)
  : apd100 H a b = ap (λs : Πx, P x → Q x, s a b) H :=
by hinduction H; reflexivity

@[hott] def apd100_of_eq_of_homotopy 
  {A : Type u} {P Q : A → Type v}
  {f g : Πx, P x → Q x} (b : homotopy2 f g)
  : apd100 (eq_of_homotopy2 b) = b :=
begin
  apply eq_of_homotopy, intro a,
  apply concat, apply (ap (λx : Π a, f a = g a, apd10 (x a))),
  apply right_inv apd10, apply right_inv apd10
end

@[hott] def eq_of_tr_eq_tr {A : Type u} {P : A → Type v} {x y : A}
  {z z' : P x}
  {p : x = y} (q : p ▸ z = p ▸ z') : z = z' :=
by hinduction p; exact q

@[hott] def sigma_eq_sigma_of_eq_cast {A A' : Type u} {B : A → Type v}
  {B' : A' → Type v} (p : A = A') (q : B' ∘ (cast p) = B)
  : (Σ a, B a) = (Σ a', B' a') :=
by { hinduction p, hinduction q, refl }

@[hott] def cast_sigma_eq_sigma_of_eq_cast {A A' : Type u} {B : A → Type v}
  {B' : A' → Type v} (p : A = A') (q : B' ∘ (cast p) = B) (x : Σ (a : A), B a)
  : cast (sigma_eq_sigma_of_eq_cast p q) x 
    = ⟨cast p x.1, hott.eq.cast ((apd10 q) x.1)⁻¹ᵖ x.2⟩ :=
by { hinduction p, hinduction q, hinduction x, refl }

@[hott] def cast_sigma_eq_sigma_of_eq_cast' {A A' : Type u} {B : A → Type v}
  {B' : A' → Type v} (p : A = A') (q : Π a, B a = B' (cast p a)) (x : Σ (a : A), B a)
  : cast (sigma_eq_sigma_of_eq_cast p (eq_of_homotopy q)⁻¹) x
    = ⟨cast p x.1, hott.eq.cast (q x.1) x.2⟩ :=
by { rwr cast_sigma_eq_sigma_of_eq_cast, rwr apd10_inv,
     rwr apd10_eq_of_homotopy, rwr hott.eq.inv_inv }

@[hott] def cast_con {A B C : Type u} (p : A = B) (q : B = C)
  : cast (p ⬝ q) = (cast q) ∘ (cast p) :=
by { hinduction q, hinduction p, refl }

@[hott] def fn_eq_fn_of_equiv_eq_equiv {A : Type u} {B : Type v}
  {f g : A ≃ B} (p : f = g) (a : A) : f a = g a :=
by { hinduction p, refl }

@[hott] def fam_homotopy_equiv_pointwise_equiv {A : Type u} (F G : A → Type v) :
  F ~ G ≃ Π (a : _), F a ≃ G a :=
begin
  fconstructor, intros H q, apply equiv_of_eq, apply H, 
  fapply adjointify,
  { intros H q, apply ua, apply H },
  { intro H, apply eq_of_homotopy, intro q, apply equiv_of_eq_ua },
  { intro H, dsimp, apply eq_of_homotopy, intro q, apply ua_equiv_of_eq }
end

@[hott, instance] def is_weakly_const_of_one_trunc
  {A : Type u} {B : Type v} [is_set A] (f : A → B) (x y : A):
  is_weakly_constant (@ap _ _ f x y) :=
begin
  intros p q, have : p = q, apply is_trunc.is_set.elim, hinduction this, refl
end

@[hott, instance] def is_weakly_const_compose {A : Type u} {B : Type v} {C : Type w}
  (f : A → B) (g : B → C) [H : is_weakly_constant g] : is_weakly_constant (g ∘ f) :=
begin
  intros p q, apply H
end

@[hott, instance] def is_prop_is_weakly_const_of_trunc {A : Type u} {B : Type v}
  [is_set B] (f : A → B):
  is_prop (is_weakly_constant f) :=
begin
  fapply is_prop.mk, intros r s, apply eq_of_homotopy, intro p,
  apply eq_of_homotopy, intro q, apply is_set.elim
end

@[hott, instance] def is_trunc_trunc_eq {A : Type u} {n : _} (x y : trunc (n+1) A) :
  is_trunc n (x = y) :=
begin
  have : is_trunc (n+1) (trunc (n+1) A), apply_instance, apply @is_trunc_eq _ _ this
end

@[hott] def weakly_const_factorization_of_conn
  {A : Type u} {B : Type v} [is_conn 0 A] [is_trunc 1 B] :
  ((trunc 0 A) → B)
  ≃ Σ (f : A → B), Π x y, is_weakly_constant (@ap _ _ f x y) :=
begin
  fconstructor,
  { intros f', fconstructor, exact f' ∘ tr, intros x y,
    intros p q, rwr [ap_compose, ap_compose], apply ap (ap f'), apply is_set.elim },
  have a0 := center (trunc 0 A), hinduction a0 with a,
  fapply adjointify,
  { intro s, hinduction s with f W, intro x, exact f a },
  { intro fW, hinduction fW with f W,
    fapply sigma_eq, apply eq_of_homotopy, intro x,
    have trax : trunc -1 (a = x), apply (tr_eq_tr_equiv -1 a x).to_fun,
    apply @is_prop.elim (trunc 0 A),
    haveI : is_prop (total_image (@ap _ _ f a x)),
    { fapply is_prop.mk, intros r s, hinduction r with p r, hinduction s with q s,
      fapply sigma_eq, apply W, apply is_prop.elimo }, resetI,
    have p : total_image (@ap _ _ f a x),
    { hinduction trax with e, fconstructor, apply ap f e,
      fapply image.mk, exact e, refl },
    exact p.1,
    apply is_prop.elimo', apply_instance },
  { intro k, apply eq_of_homotopy, intro a', apply ap k, apply is_prop.elim }
end

@[hott] def ap_rfl_factorization_of_conn {A : Type u} 
  {B : Type v} [is_conn 0 A] [is_trunc 1 B] :
  ((trunc 0 A) → B)
  ≃ Σ (f : A → B), Π a (p : a = a), ap f p = idp :=
begin
  transitivity, apply weakly_const_factorization_of_conn,
  apply sigma_equiv_sigma_right, intro f,
  apply pi_equiv_pi_right, intro a,
  fconstructor,
  { intros F p, apply F a p idp },
  fapply adjointify,
  { intros F b q r, hinduction r, apply F },
  { intro F, refl },
  { intro F, apply eq_of_homotopy, intro b, apply eq_of_homotopy,
    intro p, apply eq_of_homotopy, intro q, hinduction q, refl }
end


@[hott] def conn_comp_equiv (A : Type u) :
  A ≃ Σ (x : trunc 0 A) (a : A), tr a = x :=
begin
  fconstructor,
  { intro a, exact ⟨ tr a, a, rfl⟩ },
  fapply adjointify,
  { intro s, exact s.2.1 },
  { intro s, hinduction s with x s, hinduction s with a p, hinduction p, refl },
  { intro a, refl }
end

@[hott] def singleton_contr_equiv (A : Type u) : 
  A ≃ Σ (x y : A), x = y :=
begin
  fconstructor,
  { intro a, exact ⟨a, a, rfl⟩ },
  fapply adjointify,
  { intro s, exact s.1 },
  { intro s, hinduction s with x s, hinduction s with y p, hinduction p, refl },
  { intro a, refl }
end

@[hott] def curry_equiv (A : Type u) (B : A → Type v) (C : Π a, B a → Type w) :
  (Π (r : Σ a, B a), C r.1 r.2) ≃ (Π a (b : B a), C a b) :=
begin
  fconstructor,
  { intros f a b, exact f ⟨a, b⟩ },
  fapply adjointify,
  { intros g s, exact g s.1 s.2 },
  { intros g, refl },
  { intros f, apply eq_of_homotopy, intro s, hinduction s with a b, refl }
end

@[hott] def curry_equiv_nondep (A : Type u) (B : A → Type v) (C :Type w) :
  ((Σ a, B a) → C) ≃ (Π a, B a → C) :=
curry_equiv A B (λ _ _, C)

@[hott] def pathover_lhs {A : Type u}
  {a a' a'' : A} {p : a = a'} {q : a = a''} {r : a' = a''} (po : p =[r] q) :
  p ⬝ r = q :=
by { hinduction r, exact eq_of_pathover_idp po }

@[hott] def ap_sigma_eq_fst {A : Type u} {B : A → Type v} {C : Type w}
  (G : A → C) (p p' : sigma B) (e : p = p') :
  ap _ e = ap G e..1 :=
by { hinduction e, refl }

@[hott] def ap_eq_of_fn_eq {A : Type u} {B : Type v} {f f' : A → B}
  (e : f = f') {x y : A} (e' : x = y) 
  : ap f e' =[e; λ (f : A → B), f x = f y] ap f' e' :=
begin
  hinduction e, refl,
end

@[hott] def quotient.eta_aux {A : Type u} {B : Type v} {a a' : A} (e : a = a')
  (f f' : A → B) (ea : f a = f' a) (ea' : f a' = f' a')
  (sq : ea ⬝ ap f' e = ap f e ⬝ ea') :
  ea =[e; λ x, f x = f' x] ea' :=
by { hinduction e, apply pathover_idp_of_eq, exact sq ⬝ idp_con _ }

@[hott] def quotient.eta {A : Type u} {R : A → A → Type v} {B : Type w}
  (f : quotient R → B) :
  quotient.elim (λ (a : A), f (class_of R a)) 
    (λ a b (r : R a b), ap f (eq_of_rel R r)) = f :=
begin
  fapply eq_of_homotopy, intro x, hinduction x, refl,
  apply quotient.eta_aux (eq_of_rel R H)
    (λ x', quotient.elim (λ a, f (class_of R a)) (λ a b (r : R a b), ap f (eq_of_rel R r)) x'),
  dsimp, rwr idp_con, rwr elim_eq_of_rel,
end

@[hott] def ap_dpair_eq_dpair {A : Type u} {B : A → Type u} {C : Type w}
  (f : (sigma B) → C) {a a' : A} (e : a = a') {b : B a} {b' : B a'} (e' : b =[e] b') :
  ap f (dpair_eq_dpair e e') 
  = apd011 (λ (a : A) (b : B a), f ⟨a, b⟩) e e' :=
by { hinduction e', refl }

@[hott] def apd011_const_eq_ap {A : Type u} {B : A → Type u} {C : Type w}
  (f : A → C) {a a' : A} (e : a = a') {b : B a} {b' : B a'} (e' : b =[e] b') :
  @apd011 A C B _ _ _ _ (λ a b, f a) e e' = ap f e :=
by { hinduction e, hinduction e', refl }

@[hott] def apd_011_coh {A : Type u} {B : A → Type u} {C : Type w}
  (f l : Π a, B a → C) (g : ∀ a b, f a b = l a b)
  {a a' : A} (e : a = a') {b : B a} {b' : B a'} (e' : b =[e] b') :
  apd011 f e e' = g a b ⬝ apd011 l e e' ⬝ (g a' b')⁻¹ᵖ :=
by { hinduction e', change idp = _ ⬝ idp ⬝ _, rwr [con_idp, eq.con.right_inv] }

@[hott] def one_trunc_fn_factor_aux {A : Type u} {B : Type v} [is_trunc 1 B] :
  (Σ (f : Π (a : trunc 0 A), (Σ (a_1 : A), tr a_1 = a) → B),
    Π (a : trunc 0 A) (a_1 : Σ a_1, tr a_1 = a) (p : a_1 = a_1), ap (f a) p = idp)
  ≃ Σ (f : A → B), Π a (p : a = a), ap f p = refl (f a) :=
begin
  fconstructor,
  { intro F, fconstructor, intro a, exact F.1 (tr a) ⟨a, idp⟩, intros a0 e,
    have e' : idp =[e; λ (a_1 : A), tr a_1 = @tr 0 A a0] idp,
    { apply pathover_of_tr_eq, apply is_set.elim },
    have F' := F.2 (tr a0) ⟨a0, idp⟩ (dpair_eq_dpair e e'), dsimp at *,
    rwr ←F', clear F',
    rwr ap_dpair_eq_dpair, rwr ←apd011_const_eq_ap _ e e', dsimp,
    have G : (Σ (g : ∀ a b, F.fst (tr a0) ⟨a, b⟩ = F.fst (tr a) ⟨a, idp⟩),
      g a0 (refl (tr a0)) = idp),
    { fconstructor, intros a b, fapply apd011 F.fst, symmetry, assumption,
      revert b, hgeneralize ex : tr a0 = x, intro b, hinduction b, refl, refl },
    rwr apd_011_coh (λ a b, F.fst (tr a0) ⟨a, b⟩) (λ a b, F.fst (tr a) ⟨a, idp⟩) G.1,
    rwr apd011_const_eq_ap, have := G.2, rwr [this, idp_con] },
  fapply adjointify,
  { intros G, fconstructor, intros x p, exact G.1 p.1,
    intros x p e, dsimp, rwr ← G.2 p.1 e..1, apply ap_sigma_eq_fst },
  { intros G, dsimp, fapply sigma_eq, refl, apply is_prop.elimo },
  { intros F, fapply sigma_eq,
    fapply eq_of_homotopy, intro x, fapply eq_of_homotopy, intro p,
    dsimp, hinduction p with a e, hinduction e, refl,
    apply is_prop.elimo  }
end

-- CvW Theorem 11
@[hott] def one_trunc_fn_factor' {A : Type u} {B : Type v} [is_trunc 1 B] :
    ((trunc 0 A) → B)
    ≃ Σ (f : A → B), Π a (p : a = a), ap f p = idp :=
begin
  transitivity, apply arrow_equiv_arrow_left, apply trunc_equiv_trunc,
    apply conn_comp_equiv,
  transitivity, apply arrow_equiv_arrow_left, apply trunc_sigma_equiv_of_is_trunc,
  transitivity, apply curry_equiv_nondep,
  haveI : Π x, is_conn 0 (Σ (a : A), @tr 0 A a = x),
  { intro x, hinduction x with b, fapply is_contr.mk,
    exact tr ⟨b, rfl⟩,
    intro r, hsimp, hinduction r with r, hinduction r with a p,
    have := (tr_eq_tr_equiv _ _ _).to_left_inv p, rwr ←this, clear this,
    have : ∀ p',(tr ⟨b, refl (tr b)⟩ : trunc 0 (Σ (a : A), @tr 0 A a = tr b))
       = tr ⟨a, (tr_eq_tr_equiv -1 a b).to_inv p'⟩,
    { clear p, intro p', hinduction p' with p', hinduction p', refl },
    apply this }, resetI,
  have : Π (x : trunc 0 A),
    (trunc 0 (Σ (a : A), @tr 0 A a = x) → B)
    ≃ (Σ (f : (Σ (a : A), @tr 0 A a = x) → B), Π a (p : a = a), ap f p = idp),
  { intro x, apply ap_rfl_factorization_of_conn },
  transitivity, apply pi_equiv_pi_right, intro x, apply this, clear this,
  transitivity, symmetry, apply sigma_pi_equiv_pi_sigma,
  apply one_trunc_fn_factor_aux
end

@[hott] def hd {A : Type u} (xs : list A) : option A :=
by { hinduction xs, exact none, exact some hd }

@[hott] def tl {A : Type u} (xs : list A) : list A :=
by { hinduction xs, exact [], exact tl }

namespace option

@[hott] protected def code {A : Type u} (x y : option A) : Type u :=
begin
  hinduction x,
  { hinduction y, exact ulift unit, exact ulift empty },
  { hinduction y, exact ulift empty, exact val = val_1 }
end

@[hott] protected def decode {A : Type u}
  (x y : option A) (c : option.code x y) : x = y :=
by { hinduction x; hinduction y, refl, apply empty.elim, exact ulift.down c,
     apply empty.elim, exact ulift.down c, apply ap some, exact c }

@[hott] protected def encode {A : Type u} (x y : option A)
  (e : x = y) : option.code x y :=
by { hinduction e, hinduction x, fconstructor, fconstructor, fconstructor }

@[hott] def some_inj {A : Type u} {a b : A} (e : some a = some b) : a = b :=
option.encode (some a) (some b) e

end option

@[hott] def list.cons_inj {A : Type u} {x x' : A} {xs xs' : list A} :
  x :: xs = x' :: xs' → (x = x') × (xs = xs') :=
begin
  intros e, fconstructor,
  have : hd (x :: xs) = hd (x' :: xs'), apply ap hd, assumption,
  apply option.some_inj this,
  change tl (x :: xs) = tl (x' :: xs'), apply ap tl, assumption
end

@[hott] def list.nil_neq_cons {A : Type u} {x : A} {xs : list A} :
  ¬ ([] = x :: xs) :=
by { intro e, apply ulift.down, apply option.encode none (some x), exact ap hd e }

@[hott] def list.nil_neq_append_singleton {A : Type u} {xs : list A} {x : A} :
  ¬ ([] = xs ++ [x]) :=
by { intro e, hinduction xs; apply list.nil_neq_cons e }

@[hott] def list.append_cons_assoc {A : Type u} (xs ys : list A) (x : A) :
  xs ++ (x :: ys) = (xs ++ [x]) ++ ys :=
by { hinduction xs with y xs, refl, dsimp, rwr ih }

@[hott] def list.cons_append_assoc {A : Type u} (xs ys : list A) (x : A) :
  x :: (xs ++ ys) = (x :: xs) ++ ys :=
rfl

@[hott] def list.append_assoc {A : Type u} (xs ys zs : list A) :
  xs ++ (ys ++ zs) = (xs ++ ys) ++ zs :=
by { hinduction xs with x xs, refl, dsimp, rwr ih }

@[hott] def list.append_nil {A : Type u} (xs : list A) : xs ++ [] = xs :=
by { hinduction xs, refl, rwr ←list.cons_append_assoc, rwr ih }

@[hott] def list.nil_append {A : Type u} (xs : list A) : [] ++ xs = xs := idp

@[hott] def list.cancel_left {A : Type u} (xs : list A) {ys zs : list A}
  (p : xs ++ ys = xs ++ zs) : ys = zs :=
by { hinduction xs with x xs, exact p, exact ih (list.cons_inj p).2 }

@[hott] def list.cancel_right_singleton {A : Type u} {xs ys : list A} {y : A}
  (p : xs ++ [y] = ys ++ [y]) : xs = ys :=
begin
  revert ys, hinduction xs with x xs ih; intros ys p; hinduction ys with y ys ih',
  { refl },
  { apply empty.elim, apply list.nil_neq_append_singleton ((list.cons_inj p).2) },
  { apply empty.elim, apply list.nil_neq_append_singleton ((list.cons_inj p⁻¹).2) },
  { rwr [(list.cons_inj p).1, ih (list.cons_inj p).2] }
end

@[hott] def list.cancel_right {A : Type u} {xs ys zs : list A}
  (p : xs ++ ys = zs ++ ys) : xs = zs :=
begin
  revert xs zs, hinduction ys with y ys ih; intros xs zs p,
  { rwr [list.append_nil, list.append_nil] at p, exact p },
  { rwr [list.append_cons_assoc, list.append_cons_assoc zs] at p,
    apply list.cancel_right_singleton (ih p) }
end

@[hott] def list.length_append {A : Type u} (xs ys : list A) :
  list.length (xs ++ ys) = list.length ys + list.length xs :=
begin
  hinduction xs with x xs ih,
  { refl },
  { change list.length (xs ++ ys) + 1 = _, rwr ih }
end

@[hott] def tl_append {A : Type u} (x : A) (xs ys : list A) :
  tl ((x :: xs) ++ ys) = xs ++ ys :=
idp

end hott