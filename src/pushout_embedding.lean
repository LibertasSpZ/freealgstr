import .pushout_paths
import hott.types.sigma

universes u v w

hott_theory
namespace hott

namespace pushout

open hott hott.eq hott.is_equiv hott.is_trunc hott.function hott.sigma

section
parameters {A : Type u} {B : Type v} {C : Type w}
  (f : A → B) (g : A → C) [is_embedding f]
include f g

local notation `D ` := pushout f g
local notation `ι ` := (quotient.class_of _ : B ⊎ C → D)
local notation `inl' ` := @inl A B C f g
local notation `inr' ` := @inr A B C f g

section
parameters (c₀ : C)

@[hott] private def ap_inr_inv_Q (x : B ⊎ C) (q : inr c₀ = ι x): Type _ :=
begin
  hinduction x with b c,
  { exact Σ (a : fiber f b), fiber (ap inr') (q ⬝ ap inl' a.2⁻¹ ⬝ (glue a.1)) },
  { exact fiber (ap inr') q }
end

@[hott] private def ap_inr_inv_Qrefl
  : ap_inr_inv_Q (sum.inr c₀) idp :=
begin
  fconstructor, refl, refl
end

local attribute [instance] is_prop_fiber_of_is_embedding

@[hott, instance] private def is_contr_fiber_of_embedding_image (a : A)
  : is_contr (fiber f (f a)) :=
begin
  apply is_contr_of_inhabited_prop,
  fconstructor, exact a, refl
end

@[hott] private def ap_inl_inv_Qcons (a : A) (p : inr (c₀) = inl (f a))
  : ap_inr_inv_Q (sum.inl (f a)) p ≃ ap_inr_inv_Q (sum.inr (g a)) (p ⬝ glue a) :=
begin
  letI := is_contr_fiber_of_embedding_image f g a, --why is this needed
  exact sigma_equiv_of_is_contr_left 
    (λ (a : fiber f (f a)), fiber (ap inr) (p ⬝ ap inl (a.2)⁻¹ ⬝ glue (a.1)))
end

@[hott] private def ap_inr_inv' (c : C) (q : inr' c₀ = inr' c) 
  : ap_inr_inv_Q (sum.inr c) q :=
pushout.path_rec f g (sum.inr c₀)
 ap_inr_inv_Q ap_inr_inv_Qrefl (λ a p, ap_inl_inv_Qcons a p) (sum.inr c) q

@[hott] private def ap_inr_inv (c : C) (q : inr' c₀ = inr' c) : c₀ = c :=
(ap_inr_inv' c q).1

@[hott] private def ap_inr_of_ap_inr_inv (c : C) (q : inr' c₀ = inr' c)
 : ap inr (ap_inr_inv c q) = q :=
(ap_inr_inv' c q).2

@[hott] private def ap_inr_inv_of_ap_inr (c : C) (p : c₀ = c) 
  : ap_inr_inv c (ap inr' p) = p :=
by { hinduction p, refl }

end

@[hott] protected def preserves_embedding : is_embedding inr' :=
λ c c', adjointify _ (ap_inr_inv c c')
  (ap_inr_of_ap_inr_inv c c') (ap_inr_inv_of_ap_inr c c')

end

end pushout

end hott