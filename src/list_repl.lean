import .util .wf

open hott.acc hott.eq

universes u v w

hott_theory

namespace hott
namespace list_repl

  variables {A : Type u} (R : A → A → Type (max u v))

@[hott] inductive list_rel : list A → A → Type (max u v)
| nil : ∀ x, list_rel [] x
| cons : ∀ x y ys, list_rel ys x → R y x → list_rel (y :: ys) x

@[hott] def list_rel_append {x : A} {xs ys : list A} (r : list_rel R xs x)
  (s : list_rel R ys x) : list_rel R (xs ++ ys) x :=
begin
  hinduction r,
  { assumption },
  { hinduction s, rwr list.append_nil,
    { fconstructor, assumption, assumption },
    { rwr ←list.cons_append_assoc, fconstructor, apply ih_1,
      fconstructor, assumption, assumption, assumption } }
end

-- CvW Definition 28
@[hott] inductive list_repl : list A → list A → Type (max u v)
| replace : ∀ x xs ys zs, list_rel R xs x → list_repl (zs ++ xs ++ ys) (zs ++ x :: ys)

@[hott] protected def acc_nil : acc (list_repl R) [] :=
begin
  fconstructor, intros xs, hgeneralize eys : list.nil = ys,
  intro r, hinduction r, 
  apply empty.elim, 
  hinduction zs, apply list.nil_neq_cons, exact eys,
  apply list.nil_neq_cons, exact eys
end

@[hott] def list_repl_of_list_rel {xs : list A} {x : A} (r : list_rel R xs x) :
  list_repl R xs [x] :=
by rwr ←(list.append_nil xs); exact list_repl.replace x xs [] [] r

@[hott] def list_rel_of_list_repl {xs : list A} {y : A} :
  list_repl R xs [y] → list_rel R xs y :=
begin
  hgeneralize ey : [y] = y',
  intro r, hinduction r, hinduction zs with z zs,
  rwr [(list.cons_inj ey).1, ←(list.cons_inj ey).2, list.append_nil], assumption,
  apply empty.elim, hinduction zs with z' zs;
  apply list.nil_neq_cons; exact ap tl ey
end

@[hott] def list_repl_cons {xs ys : list A} {x : A} (r : list_repl R xs ys) :
  list_repl R (x :: xs) (x :: ys) :=
by hinduction r with x'; exact list_repl.replace x' xs ys (x :: zs) a

@[hott] def list_repl_append_right {xs ys zs : list A} (r : list_repl R xs ys) :
  list_repl R (xs ++ zs) (ys ++ zs) :=
begin
  hinduction r with x xs' ys' zs' r,
  rwr [←list.append_assoc, ←list.append_assoc _ (x :: _), ←list.cons_append_assoc],
  exact list_repl.replace x xs' (ys' ++ zs) zs' r
end

@[hott] def list_repl_append_left {xs ys zs : list A} (r : list_repl R xs ys) :
  list_repl R (zs ++ xs) (zs ++ ys) :=
begin
  hinduction r with x xs' ys' zs' r,
  rwr [←list.append_assoc, list.append_assoc, list.append_assoc, list.append_assoc],
  exact list_repl.replace x xs' ys' (zs ++ zs') r,
end

@[hott] def list_rel_inv {x a : A} {xs : list A} (r : list_rel R (x :: xs) a) :
  (R x a) × (list_rel R xs a) :=
begin
  revert r, hgeneralize exs' : x :: xs = xs', intro r, hinduction r,
  { apply empty.elim, apply list.nil_neq_cons exs'⁻¹ᵖ },
  { fconstructor,
    { rwr (list.cons_inj exs').1, assumption },
    { rwr (list.cons_inj exs').2, assumption } }
end

@[hott] def list_repl_pair_of_list_rel {x a : A} {xs : list A}
  (r : list_rel R (x :: xs) a) : list_repl R (x :: xs) [x, a] :=
by { rwr ←list.append_nil xs, 
     exact list_repl.replace a xs [] [x] (list_rel_inv R r).2 }

-- CvW Lemma 29
@[hott] def nested_acc_ind (P : A → A → Type w)
  (H : Π a0 b0, acc R a0 → acc R b0 → (Π a (r : R a a0), P a b0) 
                          → (Π b (r : R b b0), P a0 b) → P a0 b0)
  : Π (a0 : A) (aca0 : acc R a0) (b0 : A) (acb0 : acc R b0), P a0 b0 :=
begin
  intros a0 aca0, hinduction aca0, dsimp at *,
  intros b0 acb0, hinduction acb0, dsimp at *,
  apply H,
  fconstructor, assumption, fconstructor, assumption,
  { intros a r, apply ih, assumption, fconstructor, assumption },
  { exact ih_1 }
end

@[hott] def repl_descent_cons (x : A) (l ys : list A) (r : list_repl R l (x :: ys)) :
  (Σ xs', list_repl R xs' [x] × l = xs' ++ ys)
  ⊎ (Σ ys', list_repl R ys' ys × l = x :: ys') :=
begin
  revert r, hgeneralize exys : x :: ys = xys, intro r,
  hinduction r with x' xs ys' zs r,
  hinduction zs,
  { dsimp at *, hinduction (list.cons_inj exys).1, hinduction (list.cons_inj exys).2,
    apply sum.inl, fconstructor, exact xs,
    fconstructor, apply list_repl_of_list_rel, exact r, refl },
  { dsimp at *, hinduction (list.cons_inj exys).1,
    apply sum.inr, fconstructor, exact tl ++ xs ++ ys',
    fconstructor, rwr (list.cons_inj exys).2,
    fconstructor, assumption, refl }
end

@[hott] def repl_descent_append (l xs ys : list A) (r : list_repl R l (xs ++ ys)) :
  (Σ xs', list_repl R xs' xs × l = xs' ++ ys)
  ⊎ (Σ ys', list_repl R ys' ys × l = xs ++ ys') :=
begin
  revert l ys, hinduction xs with x xs'' IH,
  { intros, apply sum.inr, fconstructor, exact l, fconstructor, exact r, refl },
  { intros l ys r, dsimp at *,
    have := repl_descent_cons R x l (xs'' ++ ys) r,
    hinduction this with p p,
    { hinduction p with xs''' p, hinduction p with r''' e''',
      apply sum.inl, fconstructor, exact xs''' ++ xs'',
      fconstructor,
      change list_repl R _ ([x] ++ xs''), apply list_repl_append_right R r''',
      rwr ←list.append_assoc, assumption },
    { hinduction p with ys''' p, hinduction p with r''' e''', rwr e''' at r,
      have := IH ys''' ys r''',
      hinduction this with q q,
      { hinduction q with xs''' q, hinduction q with s exs''',
        apply sum.inl, fconstructor, exact x :: xs''',
        fconstructor, apply list_repl_cons R s,
        rwr e''', rwr exs''' },
      { hinduction q with ys'' q, hinduction q with s eys'',
        apply sum.inr, fconstructor, exact ys'',
        fconstructor, assumption, rwr ←eys'', assumption } } }
end

-- CvW Lemma 30
@[hott] protected def acc_append (xs ys : list A)
  (acxs : acc (list_repl R) xs) (acys : acc (list_repl R) ys) :
  acc (list_repl R) (xs ++ ys) :=
begin
  apply nested_acc_ind (list_repl R) (λ xs ys, acc (list_repl R) (xs ++ ys)),
  intros xs ys acxs acys H1 H2, dsimp at *,
  fconstructor, intros l r, hinduction repl_descent_append R l _ _ r with e p p,
  { rwr p.2.2, apply H1, exact p.2.1 },
  { rwr p.2.2, apply H2, exact p.2.1 },
  assumption, assumption
end

-- CvW Lemma 31
@[hott] protected def acc_singleton (x : A) (acx : acc R x) : acc (list_repl R) [x] :=
begin
  hinduction acx with acx _ IH', dsimp at *,
  fconstructor, intros l r,
  have r' := list_rel_of_list_repl R r, clear r, hinduction r' with y x y ys r s IH,
  { apply list_repl.acc_nil },
  { change acc _ ([y] ++ ys), apply list_repl.acc_append,
    { apply IH', assumption },
    { apply IH, exact IH', exact a } }
end

-- CvW Lemma 32
@[hott] def acc_list_repl (H : well_founded R) {xs : list A}
 : acc (list_repl R) xs :=
begin
  hinduction xs with x xs IH,
  { apply list_repl.acc_nil },
  { change acc _ ([x] ++ xs), apply list_repl.acc_append,
    apply list_repl.acc_singleton, apply wf_acc H x, assumption }
end

end list_repl
end hott