import hott.hit.pushout

universes u v w

hott_theory
namespace hott

namespace pushout

open hott hott.pushout hott.is_equiv hott.eq

section
  parameters {TL : Type u} {BL  : Type v} {TR : Type w}
    (f : TL → BL) (g : TL → TR)

@[hott, instance] protected def is_equiv_inl [is_equiv g] :
  is_equiv (inl : BL → pushout f g) :=
begin
  fapply adjointify,
  { intro x, hinduction x with b c, exact b,
    exact f (g⁻¹ᶠ c), apply ap f, symmetry, apply left_inv },
  { intro x, hinduction x with b c a,
    { refl },
    { transitivity, apply glue,
      apply ap inr, apply right_inv },
    { apply eq_pathover,
      refine _ ⬝hp (ap_id _)⁻¹ᵖ,
      apply move_bot_of_left, apply move_left_of_top',
      refine (con_idp _) ⬝pv _,
      refine (ap_inv _ _)⁻¹ ⬝pv _,
      refine (ap_compose inl _ _ ) ⬝pv _,
      refine (ap (ap inl) (ap_inv _ _)⁻¹ᵖ)⁻¹ ⬝pv _,
      rwr [elim_glue, ap_inv], dsimp, rwr[ap_inv, ap_inv, hott.eq.inv_inv], -- TODO replace by pv
      refine _ ⬝vp (ap (ap inr) (adj g a)⁻¹ᵖ),
      apply transpose, apply pushout.glue_square f g (left_inv g a) } },
  { intro b, refl }
end

@[hott] protected def equiv_bl_pushout [is_equiv g] : BL ≃ pushout f g :=
equiv.mk _ (pushout.is_equiv_inl f g)

end

@[hott] protected def equiv_tr_pushout
  {TL : Type u} {BL  : Type v} {TR : Type w}
  (f : TL → BL) (g : TL → TR) [is_equiv f] :
  TR ≃ pushout f g :=
pushout.equiv_bl_pushout g f ⬝e pushout.symm g f

end pushout

end hott